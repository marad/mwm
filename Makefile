include config.mk

STATUSBAR_SCRIPTS := \
	battery \
	clock \
	volume \
	weather

config_files := \
	dmenu/config.h \
	dwm/config.h \
	dwmblocks/blocks.h

ifneq ($(GENTOO),)
STATUSBAR_SCRIPTS += portage
dwmblocks/blocks.h: CPPFLAGS := -DGENTOO=1
endif

all: $(config_files) statusbar-scripts/weather dmenu dwm dwmblocks

statusbar-scripts/weather: statusbar-scripts/weather.tmpl
	sed -re 's/@WEATHER_IN@/$(WEATHER_IN)/g' $< >$@

dmenu/config.h: configs/dmenu-config.h
	cp -f $< $@

dmenu: dmenu/config.h
	make PREFIX=$(PREFIX) -C dmenu

dwm/config.h: configs/dwm-config.h
	cp -u $< $@

dwm: dwm/config.h
	make PREFIX=$(PREFIX) -C dwm

dwmblocks/blocks.h: configs/dwmblocks-blocks.h
	cpp -DPREFIX='"$(PREFIX)"' $(CPPFLAGS) $< $@

dwmblocks: dwmblocks/blocks.h
	make PREFIX=$(PREFIX) -C dwmblocks

install: all
	mkdir -p $(PREFIX)/bin/statusbar
	install $(addprefix statusbar-scripts/,$(STATUSBAR_SCRIPTS)) $(PREFIX)/bin/statusbar
	make PREFIX=$(PREFIX) -C dmenu install
	make PREFIX=$(PREFIX) -C dwm install
	make PREFIX=$(PREFIX) -C dwmblocks install

clean:
	rm -r $(config_files)
	make -C dmenu clean
	make -C dwm clean
	make -C dwmblocks clean

.PHONY: all dmenu dwm dwmblocks install
