//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"", PREFIX "/bin/statusbar/volume",						0,		5},
	{"", PREFIX "/bin/statusbar/weather",						1800,		4},
#ifdef GENTOO
	{"\uf49e ", PREFIX "/bin/statusbar/portage",					3600,		3},
#endif
	{"", PREFIX "/bin/statusbar/battery",						5,		2},
	{"", PREFIX "/bin/statusbar/clock",						1,		1},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
//static char delim[] = "|";
// U+2502
static char delim[] = "|";
//static unsigned int delimLen = 1;
static unsigned int delimLen = 2;

