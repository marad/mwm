To complete the installation process, add this to ~/.xsession:

```bash
export PATH="${HOME}/.local/bin:${PATH}"
export MANPATH="${HOME}/.local/share/man:${MANPATH}"

dwmblocks &
sxhkd &
nm-applet &
feh --no-fehbg --bg-fill /path/to/your/wallpaper # requires feh
exec dbus-launch --exit-with-session ssh-agent dwm
```

For volume keys to work, add this to ~/.config/sxhkd/sxhkdrc (requires sxhkd):

```
XF86AudioLowerVolume
	pactl set-sink-volume 0 '-5%'; kill -39 $(pidof dwmblocks)

XF86AudioRaiseVolume
	pactl set-sink-volume 0 '+5%'; kill -39 $(pidof dwmblocks)

XF86AudioMute
	pactl set-sink-mute 0 toggle; kill -39 $(pidof dwmblocks)
```
